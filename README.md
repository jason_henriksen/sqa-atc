# sqa-atc

Air Traffic Control project for SQA ticket tracking

This project provides a single view for all of our tickets for a given release across multiple ticketing systems.

# Docker Installation
This file has two docker files: one for the app, and one for the CORS proxy needed to allow the app to contact our 3rd party providers.

WARNING: on CC_Corp_Secure the docker builds always hang when trying to install npm dependencies.  I'm not clear on why.  However if I create a mobile hot spot and allow the npm install to run on that network, it goes fine every time.

My builds:

docker build -t sqa-atc -f DockerfileSQA-ATC-App .  
docker build -t sqa-atc-cors -f DockerfileSQA-ATC-Cors .  

My runs:

docker run --rm -it --network host sqa-atc  
docker run --rm -it --network host sqa-atc-cors  

NOTES:

-- this uses the '--network host' because it makes calls outside of our network.  Our network firewall will shut down the calls to git/jira/aha without this.  
-- this includes the react built version of the code so that we don't have to make docker to the building  
