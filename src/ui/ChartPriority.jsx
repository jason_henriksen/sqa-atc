import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';

@inject("CoreStore","GitTicketStore","JiraTicketStore","ReportStore") @observer
export default class ChartPriority extends Component {
  constructor(props){super(props);autoBind(this);}

  render() {

// For some reason this is not fitting into the page correctly.  It's a little overkill anyway, so I'm setting it aside for now.

    const ps = this.props.ReportStore.percentByType;
    if(!ps.total){
      return null;
    }

    return (
      <div style={{maxWidth:'99%',width:'99%',maxHeight:'32px'}} >
        <div style={{display:'inline-block',backgroundColor:'red',width:(100*(ps.blocker/ps.total))+'%',maxHeight:'24px'}}>&nbsp;&nbsp;<img src='https://clearcapital.atlassian.net/images/icons/priorities/blocker.svg' alt='blocker' title='critical' style={{width:'20px',height:'20px',padding:'2px'}}/></div>
        <div style={{display:'inline-block',backgroundColor:'darkred',width:(100*(ps.critical/ps.total))+'%',maxHeight:'24px'}}>&nbsp;&nbsp;<img src='https://clearcapital.atlassian.net/images/icons/priorities/critical.svg' alt='critical' title='critical' style={{width:'20px',height:'20px',padding:'2px'}}/></div>
        <div style={{display:'inline-block',backgroundColor:'lightcoral',width:(100*(ps.high/ps.total))+'%',maxHeight:'24px'}}>&nbsp;&nbsp;<img src='https://clearcapital.atlassian.net/images/icons/priorities/high.svg' alt='high' title='critical' style={{width:'20px',height:'20px',padding:'2px'}}/></div>
        <div style={{display:'inline-block',backgroundColor:'orange',width:(100*(ps.medium/ps.total))+'%',maxHeight:'24px'}}>&nbsp;&nbsp;<img src='https://clearcapital.atlassian.net/images/icons/priorities/medium.svg' alt='medium' title='critical' style={{width:'20px',height:'20px',padding:'2px'}}/></div>
        <div style={{display:'inline-block',backgroundColor:'peru',width:(100*(ps.low/ps.total))+'%',maxHeight:'24px'}}>&nbsp;&nbsp;<img src='https://clearcapital.atlassian.net/images/icons/priorities/low.svg' alt='low' title='critical' style={{width:'20px',height:'20px',padding:'2px'}}/></div>
        <div style={{display:'inline-block',backgroundColor:'purple',width:(100*(ps.other/ps.total))+'%',maxHeight:'24px'}}></div>
      </div>
    );
  }
}
