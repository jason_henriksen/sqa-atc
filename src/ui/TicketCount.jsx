import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';

import ReleaseKey from './ReleaseKey';
import ReleaseTargetList from './ReleaseTargetList';
import IssueList from './IssueList';
import ChartDate from './ChartDate';
import ChartComplete from './ChartComplete';
import { toast } from 'react-toastify';

@inject("CoreStore","GitTicketStore","JiraTicketStore","AhaTicketStore") @observer
export default class TicketCount extends Component {
  constructor(props){super(props);autoBind(this);}

  componentDidMount(){
    this.props.JiraTicketStore.updateJiraCount();
    this.props.GitTicketStore.updateGitCount();
    this.props.JiraTicketStore.updateMilestones();
    this.props.GitTicketStore.updateMilestones();
    this.props.AhaTicketStore.updateProductList();
  }

  testToast() { console.log('here');toast.info("hello", { autoClose: 15000 });}

  render() {
    var knownVerList = [];    
    // jira releases
    var keySort = Array.from(this.props.JiraTicketStore.projectVerMap.keys());
    // aha releases
    keySort = keySort.concat( Array.from(this.props.AhaTicketStore.projectVerMap.keys()) );
    // dedupe using set
    keySort = [...new Set(keySort)];
    keySort.sort().reverse();
    
    for(var key of keySort){
      var bgc='';
      if(this.props.JiraTicketStore.viewVersion && key.indexOf(this.props.JiraTicketStore.viewVersion)!==-1) bgc='#42ebf4';
      knownVerList.push(
        <div style={{backgroundColor:bgc}} key={key}>
          <ReleaseKey target={key}/>
          <ReleaseTargetList target={key}/>
        </div>
      );
    }

    return (
      <div>
        <div style={{position:'absolute',top:'0px',height:'120px',left:0,right:0,borderBottom:'5px solid grey',padding:'5px'}}>
        <span style={{fontSize:'36pt'}} onClick={this.testToast}>SQA - Air Traffic Control {this.props.CoreStore.workCount!==0 && '- Loading'}</span><br/>
            <ChartDate/>
            <ChartComplete/>
        </div>
        <div style={{position:'absolute',top:'135px',bottom:0,left:0,right:'60%',paddingLeft:'5px',paddingTop:'5px',paddingBottom:'5px',overflow:'auto'}}>
          <div style={{maxWidth:'99%'}}>{knownVerList}</div>
        </div>
        <div style={{position:'absolute',top:'135px',bottom:0,left:'40%',right:0,borderLeft:'5px solid #42ebf4',padding:'5px',overflow:'auto'}}>
          <IssueList/>
        </div>
      </div>
    );
  }
}

/*
*/

