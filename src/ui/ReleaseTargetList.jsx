import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import moment from 'moment';


@inject("CoreStore","GitTicketStore","JiraTicketStore","AhaTicketStore") @observer
export default class TicketCount extends Component {
  constructor(props){super(props);autoBind(this);}

  selVer(){ 
    var key = this.props.target; //
    var idx = key.indexOf(' ');
    var name = key.substring(idx).trim();
    var dt = key.substring(0,idx).trim();
    this.props.JiraTicketStore.viewVersion = name;
    this.props.JiraTicketStore.endDate = moment(dt);
    this.props.GitTicketStore.viewVersion = name;
    this.props.AhaTicketStore.viewVersion = name;
  }

  render() {

    var key = this.props.target; //
    var list = Array.from((this.props.JiraTicketStore.projectVerMap.get(key)||[]));    
    if(!list){list=[];}
    var extra='';
    if(this.props.AhaTicketStore.projectVerMap.has(key)){
      list.push('AHA');
    }

    var idx = key.indexOf(' ');
    var name = key.substring(idx).trim();
    if(this.props.GitTicketStore.milestoneNameList.includes(name)){
      list.push('CCP-UI');
    }


    return (
      <div onClick={this.selVer} style={{display:'inline-block',lineHeight:'20px',height:'20px',width:'50%',maxWidth:'50%',whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}} 
        >{list.join(' , ')}{extra}</div>
    );
  }
}
