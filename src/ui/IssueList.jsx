import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import Issue from './Issue';

@inject("CoreStore","GitTicketStore","JiraTicketStore","AhaTicketStore") @observer
export default class TicketCount extends Component {
  constructor(props){super(props);autoBind(this);}

  render() {

    var issues=[];
    for(var g=0;g<this.props.GitTicketStore.curIssueList.length;g++){
      issues.push(<Issue key={'g'+g} ish={this.props.GitTicketStore.curIssueList[g]}/>);
    }
    for(var i=0;i<this.props.JiraTicketStore.curIssueList.length;i++){
      issues.push(<Issue key={'i'+i} ish={this.props.JiraTicketStore.curIssueList[i]}/>);
    }
    for(var a=0;a<this.props.AhaTicketStore.curIssueList.length;a++){
      issues.push(<Issue key={'a'+a} ish={this.props.AhaTicketStore.curIssueList[a]}/>);
    }

    return (
      <div>
        {issues}
      </div>
    );
  }
}
