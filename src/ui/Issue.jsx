import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';

@inject("CoreStore","GitTicketStore","JiraTicketStore","AhaTicketStore") @observer
export default class TicketCount extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable expanded=false;
  @action toggleExpand(){this.expanded=!this.expanded;}

  render() {
    var ish = this.props.ish;
    var stateIcon='';
    var stateTitle='??';
    if('Resolved'===ish.status || 'Closed'===ish.status || 'Shipped'===ish.status|| 'Ready to ship'===ish.status){stateTitle='Ready To Deploy';stateIcon=<i className="material-icons" style={{color:'green'}}>check_circle</i>}
    else if('In QA'===ish.status || 'PR Out'===ish.status){stateTitle='In QA';stateIcon=<i className="material-icons" style={{color:'orange'}}>search</i>}
    else if('In Progress'===ish.status || 'Reopened'===ish.status || 'In development'===ish.status || 'Ready To Develop'===ish.status){stateTitle='In Development';stateIcon=<i className="material-icons" style={{color:'red'}}>create</i>}
    else if('Promoted Ideas'===ish.status || 'In design'===ish.status ){stateTitle='In Design';
      stateIcon=<svg style={{width:'24px',height:'24px'}} viewBox="0 0 24 24"><path fill="#dbd113" d="M12,6A6,6 0 0,1 18,12C18,14.22 16.79,16.16 15,17.2V19A1,1 0 0,1 14,20H10A1,1 0 0,1 9,19V17.2C7.21,16.16 6,14.22 6,12A6,6 0 0,1 12,6M14,21V22A1,1 0 0,1 13,23H11A1,1 0 0,1 10,22V21H14M20,11H23V13H20V11M1,11H4V13H1V11M13,1V4H11V1H13M4.92,3.5L7.05,5.64L5.63,7.05L3.5,4.93L4.92,3.5M16.95,5.63L19.07,3.5L20.5,4.93L18.37,7.05L16.95,5.63Z" /></svg>
    }
    else if('At Risk'===ish.status ){
      stateTitle='At Risk';
      stateIcon=<svg style={{width:'24px',height:'24px'}} viewBox="0 0 24 24">
                  <path fill="#ff0000" d="M12,2L1,21H23M12,6L19.53,19H4.47M11,10V14H13V10M11,16V18H13V16" />
                </svg>
    }
    else if('Scoping'===ish.status || 'Backlog'===ish.status){
      stateTitle='Not Yet Scoped';
      stateIcon=<svg style={{width:'24px',height:'24px'}} viewBox="0 0 24 24">
                  <path fill="#18cfe0" d="M11,23A2,2 0 0,1 9,21V19H15V21A2,2 0 0,1 13,23H11M12,1C12.71,1 13.39,1.09 14.05,1.26C15.22,2.83 16,5.71 16,9C16,11.28 15.62,13.37 15,16A2,2 0 0,1 13,18H11A2,2 0 0,1 9,16C8.38,13.37 8,11.28 8,9C8,5.71 8.78,2.83 9.95,1.26C10.61,1.09 11.29,1 12,1M20,8C20,11.18 18.15,15.92 15.46,17.21C16.41,15.39 17,11.83 17,9C17,6.17 16.41,3.61 15.46,1.79C18.15,3.08 20,4.82 20,8M4,8C4,4.82 5.85,3.08 8.54,1.79C7.59,3.61 7,6.17 7,9C7,11.83 7.59,15.39 8.54,17.21C5.85,15.92 4,11.18 4,8Z" />
                </svg>      
    }
    else {stateIcon=<i className="material-icons" style={{color:'purple'}}>help_outline</i>}

    var extraInfo='';
    if(this.expanded){
      extraInfo=<div style={{marginLeft:'70px'}}>Mockup from here down:<br/>
      <button style={{backgroundColor:'green'}}>qa1</button><button style={{backgroundColor:'green'}}>qa2</button><button style={{backgroundColor:'red'}}>uat</button><button style={{backgroundColor:'red'}}>cte</button><button style={{backgroundColor:'red'}}>prod</button><br/>
      <button>Branch: asef-fsefse-s</button><br/>
      <button>Branch: asef-fsefse-s</button><br/>
      <button>Branch: asef-fsefse-s</button><br/>
      Links:&nbsp;&nbsp;<a href=''>FE Ticket 1234</a>&nbsp;&nbsp;<a href=''>BE Ticket 5678</a>&nbsp;&nbsp;<a href=''>Config Ticket 91011</a>&nbsp;&nbsp;<a href=''>Wiki</a><br/>
      Who is the QA tech for this ticket;
      </div>
    } 

    var bonusKey='';
    if(ish.targetIssue){
      bonusKey=<span>/&nbsp;<a href={ish.targetURL} target='_blank'>GIT-{ish.targetIssue}</a>&nbsp;&nbsp;</span>
    }

    return (
      <div onClick={this.toggleExpand}>
      <div style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis',lineHeight:'24px',height:'24px',verticalAlign:'middle',display:'flex',marginTop:'2px'}}>
        <img src={ish.ownerMainIcon||'https://findicons.com/files/icons/1963/colorcons_blue/128/questionmark.png'} alt={(ish.ownerMain||'Please Assign Owner')} title={(ish.ownerMain||'Please Assign Owner')} style={{width:'24px',height:'24px',minWidth:'24px',minHeight:'24px',marginRight:'2px'}}/>
        <img src={ish.qaMainIcon||'https://findicons.com/files/icons/1963/colorcons_blue/128/questionmark.png'} alt={(ish.qaMain||'Please Assign QA')} title={(ish.qaMain||'Please Assign QA')} style={{width:'24px',height:'24px',minWidth:'24px',minHeight:'24px',marginRight:'2px'}}/>
        <img src={ish.priorityIcon} alt={ish.priority} title={ish.priority} style={{width:'24px',height:'24px',minWidth:'24px',minHeight:'24px'}}/>        
        <span title={stateTitle}>{stateIcon}</span>
        <a href={ish.url} target='_blank'>{ish.key}</a>&nbsp;&nbsp;{bonusKey}{ish.summary}
      </div>
      {extraInfo}
      </div>
    );
  }
}
