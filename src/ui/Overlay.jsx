import React, { Component } from 'react';
import {observer } from 'mobx-react';

import autoBind from 'react-autobind';
import { inject } from 'mobx-react';

import '../patterns.css';

@inject("CoreStore") @observer
export default class Overlay extends Component {
  constructor(props){super(props);autoBind(this);}

  render() {
    var style={};
    //return (<div style={style}>{this.props.children}</div>);
    
    var overlay='';
    //console.log('workcount: '+this.props.CoreStore.workCount);
    style={position:'absolute',top:'0',bottom:0,left:0,right:0};
    if(this.props.CoreStore.workCount){
      overlay=
      <div>
        <div style={{position:'absolute',top:0,bottom:0,left:0,right:0,textAlign:'center'}}>&nbsp;</div>
        <div style={{position:'absolute',top:'85px',left:'50px',opacity:1,zIndex:50,backgroundColor:'white',padding:'10px',fontSize:'64px',border:'3px solid green',borderRadius:'50px',minWidth:'700px'}}>&nbsp;&nbsp;&nbsp;LOADING</div>
      </div>;
    }
    return (
      <div>      
        {overlay}
        <div style={style}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

