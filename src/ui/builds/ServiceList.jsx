import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';
import BuildHistoryGit from './BuildHistoryGit';
import BuildHistoryJira from './BuildHistoryJira';


@inject("CoreStore","GitTicketStore","JiraTicketStore","JenkinsStore") @observer
export default class ServiceList extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable tools=false;
  @action toggleExpand(){this.tools=!this.tools;}

  render() {
    // git section
    var gitServiceBuildHistory = [];
    var keySort = Array.from(this.props.JenkinsStore.gitBuildHistoryMap.keys()).sort().reverse();
    for(var g=0;g<keySort.length;g++){
      gitServiceBuildHistory.push(<BuildHistoryGit key={'g'+keySort[g]} service={keySort[g]}/>);
    }
    // jira section
    var jiraServiceBuildHistory = [];
    for(var j=0;this.props.JenkinsStore.rawBuildList && this.props.JenkinsStore.rawBuildList && j<this.props.JenkinsStore.rawBuildList.length;j++){
      jiraServiceBuildHistory.push(<BuildHistoryJira key={'j'+j} service={this.props.JenkinsStore.rawBuildList[j]}/>);
    }
    return (
      <div>
        {gitServiceBuildHistory}
        {jiraServiceBuildHistory}
      </div>
    );
  }
}