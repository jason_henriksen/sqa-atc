import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';
import ServiceList from './ServiceList';


@inject("CoreStore","GitTicketStore","JiraTicketStore","JenkinsStore") @observer
export default class BuildExplorer extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable tools=false;
  @action toggleExpand(){this.tools=!this.tools;}

  componentDidMount(){
    this.props.JenkinsStore.updateKnownBuildList();
    //this.props.JenkinsStore.updateUIBuildList();

    this.props.GitTicketStore.updateGitPrList();

    this.props.JenkinsStore.updateGitBuildHistoryMap();
  }

  render() {

    return (
      <div style={{overflow:'auto',verticalAlign:'top'}}>
        I explore the builds.  Currently git-only, Jira tomorrow.  Currently only manual builds are shown, but auto-builers should be easy to add.
        <br/><br/>
        <ServiceList/>
      </div>
    );
  }
}