import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';


@inject("CoreStore","GitTicketStore","JiraTicketStore","JenkinsStore") @observer
export default class ChangeSetItem extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable tools=false;
  @action toggleExpand(){this.tools=!this.tools;}

  render() 
  {
    var service = this.props.JenkinsStore.gitBuildHistoryMap.get(this.props.service);
    var build = service.builds[this.props.buildIdx];
    var item = build.changeSet.items[this.props.itemIdx];

    var pr = '??';
    var isPROK='white';
    pr = (this.props.GitTicketStore.gitCommitPile.get(item.commitId)||'??');
    if(pr==='??'){isPROK='red';}

    return (
      <div style={{border:'1px solid tan',backgroundColor:isPROK}}>
        {pr}&nbsp;
        <a href={'https://github.com/clearcapital/ccp/commit/'+item.commitId}>{item.msg}</a>
      </div>
    );
  }
}