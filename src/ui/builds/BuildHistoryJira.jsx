import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';
import BuildItemJira from './BuildItemJira';


@inject("CoreStore","GitTicketStore","JiraTicketStore","JenkinsStore") @observer
export default class BuildHistoryJira extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable tools=false;
  @action toggleExpand(){this.tools=!this.tools;}

  render() 
  {
    //var service = this.props.service;
    var name = this.props.service.name;
    name = name.replace('-branch','');
    name = name.replace('-builder','');

    var buildList=this.props.JenkinsStore.jiraBuildHistoryMap.get(this.props.service.name);

    var uiList=[];
    for(var g=0;g<buildList && buildList.length && g<buildList.length;g++){
      uiList.push(<BuildItemJira key={'b'+g} service={this.props.service} build={buildList[g]}/>);
    }
    

    return (
      <div>
        <div style={{display:'inline-block',whiteSpace:'nowrap',verticalAlign:'top'}}>
          {name}
          {uiList}
        </div>
      </div>
    );
  }
}

/*
        <div  style={{display:'inline-block',whiteSpace:'nowrap',verticalAlign:'top',width:'5000px'}}>
          {buildList}
        </div>
*/