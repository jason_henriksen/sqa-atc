import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';
import BuildItem from './BuildItem';


@inject("CoreStore","GitTicketStore","JiraTicketStore","JenkinsStore") @observer
export default class BuildHistoryGit extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable tools=false;
  @action toggleExpand(){this.tools=!this.tools;}

  render() 
  {
    var service = this.props.JenkinsStore.gitBuildHistoryMap.get('ccp-webapp-branch-builder');
    console.log('blmap',service);
    var buildList=[];
    for(var g=0;service && service.builds && g<service.builds.length;g++){
      console.log('bl');
      buildList.push(<BuildItem key={'b'+g} service={this.props.service} buildIdx={g}/>);
    }

    return (
      <div>
        <div style={{display:'inline-block',whiteSpace:'nowrap',verticalAlign:'top'}}>
          {this.props.service}
        </div>
        <div  style={{display:'inline-block',whiteSpace:'nowrap',verticalAlign:'top',width:'5000px'}}>
          {buildList}
        </div>
      </div>
    );
  }
}