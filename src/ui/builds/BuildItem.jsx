import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';
import moment from 'moment';
import ChangeSetItem from './ChangeSetItem';


@inject("CoreStore","GitTicketStore","JiraTicketStore","JenkinsStore") @observer
export default class BuildItem extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable tools=false;
  @action toggleExpand(){this.tools=!this.tools;}

  render() 
  {
    var service = this.props.JenkinsStore.gitBuildHistoryMap.get(this.props.service);
    var build = service.builds[this.props.buildIdx];
    var ts = moment(Number(build.timestamp));
    var itemList = [];
    var prList = [];
    var prUIList = [];

    //-- build the specific commits in the build

    for(var i=0;i<build.changeSet.items.length;i++){

      //-- build the unique PRs list
      var item = build.changeSet.items[i];
      var pr = (this.props.GitTicketStore.gitCommitPile.get(item.commitId)||'??');
      if(!prList.includes(pr)){ 
        prList.push(pr);
        prUIList.push(<a key={'pr'+i} href={'https://github.com/clearcapital/ccp/pull/'+pr}>GITPR-{pr}<br/></a>); 
      }
        
      //-- list the specific commits in the build for mouseover
      itemList.push(<ChangeSetItem 
                      key={i}
                      service={this.props.service} 
                      buildIdx={this.props.buildIdx}
                      itemIdx={i}
                    />);
    }


    return (
      <div style={{display:'inline-block',border:'1px solid grey',verticalAlign:'top',padding:'3px'}} onClick={this.toggleExpand}>
        {build.id}<br/>
        {ts.format('YYYY-MM-DD')}<br/>
        {ts.format('HH:mm')}<br/>
        <br/>
        {prUIList}
        {this.tools && 
          <div>
            <br/>
            {itemList}
          </div>
        }
        </div>
    );
  }
}