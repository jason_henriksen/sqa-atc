import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';
import TicketCount from './TicketCount';
import BuildExplorer from './builds/BuildExplorer';


@inject("CoreStore","GitTicketStore","JiraTicketStore") @observer
export default class ToolChooser extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable tools=true;
  @action toggleExpand(){console.log('click');this.tools=!this.tools;}

  render() {
    return (
      <div>
        <div style={{position:'absolute',top:0,right:0,zIndex:10,textAlign:'right'}}>
          <button onClick={this.toggleExpand}>Releases</button>
          <button onClick={this.toggleExpand}>Builds</button><br/>
          v0.1.0
        </div>
        {this.tools===false && <TicketCount/>}
        {this.tools===true && <BuildExplorer/>}
      </div>
    );
  }
}
