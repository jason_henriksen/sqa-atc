import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import { observable,action } from 'mobx';
import moment from 'moment';

@inject("CoreStore","GitTicketStore","JiraTicketStore") @observer
export default class ChartDate extends Component {
  constructor(props){super(props);autoBind(this);}

  @observable expanded=false;
  @action toggleExpand(){this.expanded=!this.expanded;}

  getStyle(offset){
    var testDate = this.props.JiraTicketStore.endDate.clone().subtract(offset,'days');
    if(testDate.isBefore(moment())){      
      if(testDate.isSame( moment(),'day')){
        return {backgroundColor:'#99e6ff',color:'black',border:'1px solid grey',width:'7%',maxWidth:'7%',paddingLeft:'3px'}
      }
      else{
        return {backgroundColor:'black',color:'white',border:'1px solid grey',width:'7%',maxWidth:'7%',paddingLeft:'3px'}
      }
    }
    else{
      var day = testDate.format('dd');
      if("Su"===day || "Sa"===day){
        return {backgroundColor:'#fff9e6',color:'black',border:'1px solid grey',width:'7%',maxWidth:'7%',paddingLeft:'3px'}
      }
      else{
        return {backgroundColor:'white',color:'black',border:'1px solid grey',width:'7%',maxWidth:'7%',paddingLeft:'3px'}
      }
    }
  }
  getText(offset){
    return this.props.JiraTicketStore.endDate.clone().subtract(offset,'days').format('dd MM-DD')
  }

  render() {
    // this.props.endDate -- Must be a moment.js object
    if(!this.props.JiraTicketStore.endDate){return null;} // no op on out of here

    return (
      <div title='These times *will* change.  They are just the default guideline.  And really this is just playing with ideas.  Eventually this could link to the real calendar.'
          style={{maxHeight:'40px'}}
      >
        <table cellSpacing='0' width='99%'><tbody><tr>
            <td style={this.getStyle(13)}><br/>{this.getText(13)}</td>
            <td style={this.getStyle(12)}><br/>{this.getText(12)}</td>
            <td style={this.getStyle(11)}><br/>{this.getText(11)}</td>
            <td style={this.getStyle(10)}><br/>{this.getText(10)}</td>
            <td style={this.getStyle(9)}><br/>{this.getText(9)}</td>
            <td style={this.getStyle(8)}><br/>{this.getText(8)}</td>
            <td style={this.getStyle(7)}><br/>{this.getText(7)}</td>
            <td style={this.getStyle(6)}><br/>{this.getText(6)}</td>
            <td style={this.getStyle(5)}><br/>{this.getText(5)}</td>
            <td style={this.getStyle(4)}>FREEZE<br/>{this.getText(4)}</td>
            <td style={this.getStyle(3)}><br/>{this.getText(3)}</td>
            <td style={this.getStyle(2)}>UAT<br/>{this.getText(2)}</td>
            <td style={this.getStyle(1)}>CTE<br/>{this.getText(1)}</td>
            <td style={this.getStyle(0)}>PROD<br/>{this.getText(0)}</td>
        </tr></tbody></table>
      </div>
    );
  }
}
