import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';

@inject("CoreStore","GitTicketStore","JiraTicketStore","ReportStore") @observer
export default class ChartComplete extends Component {
  constructor(props){super(props);autoBind(this);}

  render() {
    const ps = this.props.ReportStore.percentByStatus;
    if(!ps.total){
      return null;
    }

    return (
      <div style={{maxWidth:'99%',width:'99%',maxHeight:'28px'}} >
        {ps.other!==0 && <div title={ps.other+' tickets not ready to code'} style={{display:'inline-block',maxHeight:'29px',overflow:'hidden',backgroundColor:'purple',width:(100*(ps.other/ps.total))+'%'}}><i className="material-icons" style={{color:'white'}}>help_outline</i></div>}
        {ps.dev!==0 && <div title={ps.dev+' tickets coding'} style={{display:'inline-block',maxHeight:'29px',overflow:'hidden',backgroundColor:'red',width:(100*(ps.dev/ps.total))+'%'}}><i className="material-icons" style={{color:'white'}}>create</i></div>}
        {ps.qa!==0 && <div title={ps.qa+' tickets validating'} style={{display:'inline-block',maxHeight:'29px',overflow:'hidden',backgroundColor:'orange',width:(100*(ps.qa/ps.total))+'%'}}><i className="material-icons" style={{color:'white'}}>search</i></div>}
        {ps.done!==0 && <div title={ps.done+' tickets ready to deploy'} style={{display:'inline-block',maxHeight:'29px',overflow:'hidden',backgroundColor:'green',width:(100*(ps.done/ps.total))+'%'}}><i className="material-icons" style={{color:'white'}}>check_circle</i></div>}
      </div>
    );
  }
}
