import React, { Component } from 'react';
import {observer,inject } from 'mobx-react';
import autoBind from 'react-autobind';
import moment from 'moment';

@inject("CoreStore","GitTicketStore","JiraTicketStore","AhaTicketStore") @observer
export default class TicketCount extends Component {
  constructor(props){super(props);autoBind(this);}

  selVer(){ 
    var key = this.props.target; //
    var idx = key.indexOf(' ');
    var name = key.substring(idx).trim();
    var dt = key.substring(0,idx).trim();
    this.props.JiraTicketStore.viewVersion = name;
    this.props.JiraTicketStore.endDate = moment(dt);
    this.props.GitTicketStore.viewVersion = name;
    this.props.AhaTicketStore.viewVersion = name;
  }

  render() {

    var key = this.props.target; //this.props.JiraTicketStore.projectVerMap.get(key)
    var idx = key.indexOf(' ');
    var dt = key.substring(0,idx).trim();
    var name = key.substring(idx).trim();
    var style={};

    var pleaseFix='';
    var usedNames = this.props.JiraTicketStore.badReleaseDateList;
    if(usedNames.includes(name)){ pleaseFix=<span style={{color:'orange'}}>inconsistent dates</span>}

    if('DATE.MISSING'===dt){ style={color:'red',textOverflow:'ellipsis'}; }
    else{
      var now = moment();
      var then = moment(dt);
      var diffDays = then.diff(now,'days');
      var diffTime = diffDays;
      if(diffTime>30) diffTime=30;
      if(diffTime<0)  diffTime=30;
      var opacity = 1- (diffTime/40);
      style={opacity:opacity,textOverflow:'ellipsis'}
    }

    return (
      <div onClick={this.selVer} style={{display:'inline-block',lineHeight:'20px',height:'20px',width:'50%',maxWidth:'50%',whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis',...style}}>{dt}&nbsp;&nbsp;{name}&nbsp;&nbsp;{pleaseFix}</div>
    );
  }
}
