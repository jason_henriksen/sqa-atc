

import {observable,computed,reaction,action, autorun } from 'mobx';
import autoBind from 'react-autobind';
import moment from 'moment';
import { toast } from 'react-toastify';


class JiraTicketStore
{
  constructor(){autoBind(this);} 
  
  core = {}; // set initially in App.js.  Allows the store to make API calls

  @observable ticketCount=0;
  @observable projects={};
  @observable projectVerMap=observable.map({});
  @observable verList=observable.map({});

  @observable viewVersion='';
  @observable endDate=null;
  @observable curIssueList=[];
  @observable curTotalCount=0;  

  qaUserNameListJira=    ['Aleksandr','Billy','Corey','Jason H','Jose','Suneetha','Varshana','Victoria'];
  qaUserFullNameListJira=['aleksandr.prutyanu','billy.saetern','cory.lotze','jason.henriksen','jose.rosales','suneetha.kosaraju','varshana.lal','victoria.gustafson'];
  qaUserIconListJira=[
  'https://avatar-cdn.atlassian.com/426144d68548710608ff4643dca021c8?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426144d68548710608ff4643dca021c8%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
  'https://avatar-cdn.atlassian.com/5ee2e1779099f4496469f6c12e46a620?s=24&d=https%3A%2F%2Fclearcapital.atlassian.net%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26avatarId%3D12720%26noRedirect%3Dtrue',
  'https://avatar-cdn.atlassian.com/c418d196c14e3bd3de9f040e5ffd0b13?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fc418d196c14e3bd3de9f040e5ffd0b13%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
  'https://avatar-cdn.atlassian.com/e56bd3fafb955972fd6f01baa71a772f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fe56bd3fafb955972fd6f01baa71a772f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
  'https://avatar-cdn.atlassian.com/cd3c6f51533e0ced04723213d79bb090?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcd3c6f51533e0ced04723213d79bb090%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
  'https://avatar-cdn.atlassian.com/5239dc5083692f8c3870eab1cde1fa46?s=24&d=https%3A%2F%2Fclearcapital.atlassian.net%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26avatarId%3D12720%26noRedirect%3Dtrue',
  'https://avatar-cdn.atlassian.com/6e0d1b259d28644dcfef790410d5d47b?s=24&d=https%3A%2F%2Fclearcapital.atlassian.net%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dvarshana.lal%26avatarId%3D13100%26noRedirect%3Dtrue',
  'https://avatar-cdn.atlassian.com/8d4f3d6588e5bd11eb8e8c08858b7e59?s=48&d=https%3A%2F%2Fclearcapital.atlassian.net%2Fsecure%2Fuseravatar%3FownerId%3Dvictoria.gustafson%26avatarId%3D11800%26noRedirect%3Dtrue'];


  @computed get projectsTitles(){
    var res=[];
    for (var i = 0; this.projects && i < this.projects.length; i++) { // loop the projects
      // add to key list
      if(!this.projects[i].name.startsWith('z_')){
        // remember the project key for later
        res.push(this.projects[i].key);      
      }
    };
    return res;
  };

  @computed get projectMap(){
    var res={};
    for (var i = 0; this.projects && i < this.projects.length; i++) { // loop the projects
      // add to key list
      if(!this.projects[i].name.startsWith('z_')){
        // remember the project key for later
        res[this.projects[i].key]=this.projects[i];      
      }
    };
    return res;
  };


  loadProjectData = reaction(
    () => (this.projects && this.projects.length),  // if the number of project keys changes (usually only on load, using react() to avoid redundant work
    () => {                              // load each project
    for (var i = 0; this.projects && i < this.projects.length; i++) { // loop the projects.  Can't do this in compute for data integrity rules.
      if(!this.projects[i].name.startsWith('z_')){
        // load each project  (technically, this fetch should be done in a separate auto-run, instead of computed)
        this.updateProject(this.projects[i].key);
      }
    };
  });

  loadIssueData = autorun(()=>{
    if(this.viewVersion){
      this.curIssueList.clear();
      this.updateIssueList(this.viewVersion); // anytime view version is changed, go load the issues for that version
    }
  });

  @action updateJiraCount(){ this.core.smartFetchJiraGet("/rest/api/2/search",(res)=>{this.ticketCount = res.total;});  }  
  @action updateMilestones(){ this.core.smartFetchJiraGet("/rest/api/2/project",(res)=>{this.projects = res;}); }
  @action updateProject(key){ this.core.smartFetchJiraGet("/rest/api/2/project/"+key,(res)=>{ this.updateProjectVer(res); }); }

  @action updateIssueList(key){ this.core.smartFetchJiraGet("/rest/api/2/search/?jql="+
    encodeURI('fixVersion in (\''+this.viewVersion+'\') ORDER BY status'),(res)=>{ this.updateIssueListWithResults(res); }); }

  @action updateIssueListWithResults(res){
    if(!res){toast.error("Jira Access Error: Missing Results");}

    this.curTotalCount=res.total;

    this.curIssueList.clear();
    for(var i=0;i<res.issues.length;i++){
      var ish={};
      ish.key=res.issues[i].key;
      ish.url='https://clearcapital.atlassian.net/browse/'+ish.key;
      if(res.issues[i].fields.assignee){
        ish.ownerMain=res.issues[i].fields.assignee.key;
        ish.ownerMainIcon=res.issues[i].fields.assignee.avatarUrls['24x24'];
      }
      if(res.issues[i].fields.priority){
        ish.priority=res.issues[i].fields.priority.name;
        ish.priorityIcon=res.issues[i].fields.priority.iconUrl;
      }
      if(res.issues[i].fields.status){
        ish.statusIcon=res.issues[i].fields.status.iconUrl;
        ish.status=res.issues[i].fields.status.name;
      }
      if(res.issues[i].fields.customfield_12222){
        ish.qaMain=res.issues[i].fields.customfield_12222[0].value;
        if(this.qaUserNameListJira.includes(ish.qaMain)){
          ish.qaMainIcon=this.qaUserIconListJira[ this.qaUserNameListJira.indexOf(ish.qaMain) ];
          ish.qaMain =   this.qaUserFullNameListJira[ this.qaUserNameListJira.indexOf(ish.qaMain) ]; // order matters here
        }        
      }
      ish.summary=res.issues[i].fields.summary;
      ish.description=res.issues[i].fields.description;
      this.curIssueList.push(ish);
    }
  }

  @action updateProjectVer(res){
      // version mapping:  This could be done in a @computed, but this will cause less re-computes      
      for (var i = 0; res && i < res.versions.length; i++) { // loop the projects.  Can't do this in compute for data integrity rules.
        //this.verList.set(res.versions[i].name,'X');
        var curVer = res.versions[i];
        var sortName = (curVer.releaseDate||'DATE.MISSING')+'  '+curVer.name;
        if(!curVer.archived && !curVer.released && moment(curVer.releaseDate) > moment().add(-1,'M') ){
          if(!this.projectVerMap.has(sortName) ){ 
            this.projectVerMap.set(sortName, observable([]) );   // map vername to projects
          }
          this.projectVerMap.get(sortName).push(res.key);
        }
      }
  }

  // give a list of all release names that have more than one release date associated to them
  @computed get badReleaseDateList(){
    var list = this.projectVerMap.keys();
    var found=[];
    var dupes=[];
    var list2 = Array.from(list);
    for (var i = 0; i<list2.length; i++) { // loop the projects
      var rel = list2[i];
      var idx = rel.indexOf(' ');
      var name = rel.substring(idx).trim();
      if(!found.includes(name)){found.push(name)}
      else if(!dupes.includes(name)){dupes.push(name)}
    };
    return dupes;
  };


}

export default new JiraTicketStore();