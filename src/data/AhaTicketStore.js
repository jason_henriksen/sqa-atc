

import {observable,action, autorun } from 'mobx';
import autoBind from 'react-autobind';
import moment from 'moment';
import { toast } from 'react-toastify';


class AhaTicketStore
{
  constructor(){autoBind(this);}    

  core = {}; // set initially in App.js.  Allows the store to make API calls

  // used for release analysis
  @observable productList=[];
  @observable projectVerMap=observable.map({});

  @observable viewVersion='';
  @observable curIssueList=[];

  @action updateProductList(){
    this.core.smartFetchAhaGet("/api/v1/products",(res)=>{
      if(!res || !res.products){
        toast.error("Aha Access Error: Missing Results");        
        return;
      }
      this.productList=res.products;
      // I (c||sh)ould do this in a reaction, but I'll just make the calls directly since it's only at startup
      for(var p=0;p<this.productList.length;p++){
        this.updateProductReleases(this.productList[p].reference_prefix);
      }
    });
  }

  @action updateProductReleases(productId){
    this.core.smartFetchAhaGet("/api/v1/products/"+productId+"/releases?per_page=200",(res)=>{
      var pastLimit = moment().subtract(1,'months');
      for(var p=0;p<res.releases.length;p++){
        if( res.releases[p].name.indexOf('(')!==-1 && 
            res.releases[p].release_date &&
            pastLimit.isBefore( moment(res.releases[p].release_date)) ){
          // releases named to match a jira release are imported if they are less than 2 months past
          var sharedName = res.releases[p].name.substring(res.releases[p].name.indexOf('(')+1,res.releases[p].name.indexOf(')'));
          var sortName=res.releases[p].release_date+'  '+sharedName;
          if(!this.projectVerMap.has(sortName) ){ 
            this.projectVerMap.set(sortName, observable([]) );   // map vername to projects
          }
          this.projectVerMap.get(sortName).push(res.releases[p].reference_num);
        }

      }

    });
  }

  // when viewVersion changes, load the features for the new version.
  loadIssueData = autorun(()=>{
    if(this.viewVersion){
      this.curIssueList.clear();
      var nameList = Array.from(this.projectVerMap.keys());
      var key=''
      for(var n=0;n<nameList.length;n++){
        if(nameList[n].toLowerCase().includes(this.viewVersion.toLowerCase())){
          key=nameList[n];
        }
      }

      if(key){
        this.updateIssueList( this.projectVerMap.get(key) ); // anytime view version is changed, go load the features for that version
      }
    }
  });

  @action updateIssueList(key){ this.core.smartFetchAhaGet("/api/v1/releases/"+key+"/features?fields=id,reference_num,name,workflow_status,created_by_user,custom_fields",
    (res)=>{ this.updateIssueListWithResults(res); }); }

  @action updateIssueListWithResults(res){
    if(!res){toast.error("Aha Access Error: Missing Results");}

    this.curIssueList.clear();
    for(var i=0;i<res.features.length;i++){
      var ish={};
      ish.key=res.features[i].reference_num;
      ish.url='https://clearcapital.aha.io/features/'+ish.key;
      if(res.features[i].created_by_user){
        ish.ownerMain=res.features[i].created_by_user.name;
        ish.ownerMainIcon='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAApVBMVEUAc8////8Ab84AcM4Aa80Acs8Abc4AcM8DddD5/f4Aacz9//6ArOHs9vxHj9gAZcsAY8rR5vY9k9oAeNEuetEAfNMAYcrv9/yy0e9hnt3l8Pplo95+s+TY6/jE3vO61/Efh9aYxutzreJFjtikyOtUltqXvugwhNSlzu6KveiItOTC3/RqqeE0j9m21PA1ftJQnN0QgtWBueecvuePwemfwunC2fHkUWbHAAAKjUlEQVR4nO2bfXuiOhOHISEx0lpkqe+i9b22ttaebb//R3uCAsJMoIJr/3nm3rPX2UswyS+vk5nRsgiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIIj/JzjnP76jLNFgTP784vWNYUz8o7KU/sOl48pgc2Cq8C2NYNJ1xeHhfr3/V5UbG8Mc1wk2q/nhX3SkspjrdV3rMFpPw2GrY5SoP+PMbXdl8DRfToatZvNe/oO6TY1xdGOEbsy7r6tZX1tLQzKmNg+7v+/hnR3RtGeO4T3OxH4xGM9eh/aJu1soFIxHjdm+h824GvvlylrY4X7dn5zLi/AtNDGUXPWnfiv71i0Uik3UmF62muY9u6pI9uDnxB1pPaBCVfsvfOsWCt05akxvcJVCJaaoSN32tUQLEdd9E4U73N2rqxSKTWhQaE/Ri0oO4FjfQKGSozvYluHimr1UOSOTQHu4QeeAeOiBl26hkK1gLbotV50WbdMk1cxdNE03Q/DOLWYpX8Ba7GHnGoWC423mSL8NFYq9/wsKLbxq/OAKu0LJP2aBds+F74rHyW8ofIT9aE+uKk++Fyi0PxwwiMKC795EYQD70Z5es5WKA5r1CV/IrGH931BooY2hbzKwLoWN0M6VECq4vuWvKBSwFnuJFszlKI6KS0GWhJLL31DIYC32+gqF1t543McFQ4Vs/RsK5X+wIeP6Cg1mSoYp2GkUewFv30bhCzRqdlesQ++5WKDde8gvxN9SeA97/aN+JcLJXYZCsOuM82aNYuNShUJwLixV6B2IS9EvCEsk/8SwAdz8/tRWqORbbsw+wckxbYO658UKpdd2hFLca7vCrFFFl1v9WrftOowx6Xr6X8xCL/NVK1+L/VR/DPOb4/AJnLV34LxgI6NCZTG2Gfx9Hfbsu57fn2+QI+vktmJqsxqNZ++TMGy1hsPQn87Gq32Dgbf5Airs1D7xRSe3k/roNABmDTo8I4XKctRgmS0oXC7A5icddhi8QD/CsQS/f6+c3KhzZOAHtRWCGd9vw4vU0il7/6hQz7sPHy6c3igrkbtv4+9hr2jXbvo7l2ckNpCZhUyPS1HgbP1wO6DoYd6Nh/eA5r03QIbysW/OtwHRMb6Rxd9kRol3gMIer321ALehgElYdt6sMShcfxUMzZeXDAtD6woTZvwUIgBWSKv2RsNWudaFrkK34S+Z/wIyYgutWvstcfQwtDeaJG5SB61QYMyHdQ981Z7lCpq1lTsGFfuP2T3AoLAYX4gq3/LPK5d/Fz6qhnDzc3IkFRhVPUQ5L9dFo4G+iue2kVE6UhLMpIn3gxFRhHzKlXN0aCEPwtirqzA13NEpaiYdc8sB953XNmz6ZSj3K1fO0X3I4UL0s6WzhyoKp8ksRXamkfN0ccGxPO3WG0MR5Bf0l17qiuVVazqZo6qawsnjqSeRNWvmLg1OuOCShn1ilwGmXHMUdSG26wdOXYXhyeOqZOZG0np93n6Ox9tng+hlOoYv+QfPdRXmh2vYaegPG0/QnviW5+O2SKF5jFqny1dyb+4Nl6OD63ptz9N/LexbSK+jMHowq6lQ5c3s72MPcgvaH1l/s1Fha/r1su4bRMYL62Q5hctBx3GiYG4UYVWWxB6weFbrnQZELv7W2ksVAy76+C6IjO/e6HxeGBT2nhf6ZsEceK/SNBOFa3u620t9hTg3VCiBfIZ+qvAj/2CLve+X4G3zxTwdbQpDFKN/jgYbFC7bx4YruP9FpAdioK+OaddagjPJFfKfnxVCJ/VnvRPfy598Lfe0LfADrjhtnUFhvxE/Mhzrg2Rz5MldXgnmys7i/j/TRSrcJwrf8g/GdcZQMaDkPT73hIuM61U6iCaF8SrFl7pIYd4JYsm2fJpPw6F5ZzorzJsi9hw63y/C+QSlxP2kPOTlPy/0EoUWx1ekrMIoy2M/mBU62LMKYe+PaikU+Qa1FpHrRMO70Pi2w9RwM9yDEoVCfsNHWYWKycXaEEw3KwTX1BEOR/8MbGtv2k9Ae9zZTVKiEG/CWYXCC/o/GgthstOwoKicy1FyjULJxaQrvWyWwmlvZ/pe7mcXVJPupVzlB7tWnoLCc6qYSZJ1ZVD4nirEKRSJQrn60Y2RUyhEfrerk6eAHXZlpGZNyRia0gFihe7usspShRbLW8w1FCoPT6lizm5f0xiKROEH+uJJobtFD35S6OS23DoKLe+1gsJooNQlCuHSjhQqaUj1aIaT9yXa0VK71HJn/kQznb6/661v3anuauOqkkDb3xeO4TRViPMBIoUMWwLD7Wr/aHlo3Z4VWtbjYxClQEaREF4j91IZEqvKGcmaCoUFt7TmzvV0m5WDzt2MwvSUERaOa1yA4PjMKye+oVVXiOKTvbl3ajRy69mTmv4mAyYbspzwZN0bFH6XKNw5qg3jk0mP3FYhCnT+zNNxWzQoTJplVig4sOPP0bibKhSFKTSFfDpFCoMyhQzcEjJhghKFIG4qCuKRJQLhJG22AHiIJ8fgiEnhY7HCuYOOit7uZ4XC9brd9oluu+s1rIpxC7yTfrmNHM4flIN4Sqg1KPRLFI4dvGEuE1vVoDD2PlrPrdB/PeGHw1avahwfhV+a+iwQWTj2MDSPzkyDz9uPk/WNCl1sjrdWHo8cGU6AnG1xZ6lHlAAzr+bG4A1ge+D8VIUTMvuiusJPF4+h3ftgXtsJxviymEwHnLk3qhR8wiayr4DRoO9WqGHHhFqDwtjta1S49STOaNYa/VdjmlKxwoqzlMEBwlljpmS3yJVgTN8tVvjXY0WpnSaSO/61Cjlyxe6QK8tkEkSudZPCRbHCmcdh2LxUYdxZWGG1fBocmcC5HAJ5vvXckiJSCO8Pya8WTAp1p7glaXOQZD/ACt+qKFQot7Else3uGPy7b1EAFSnslSjsd2GYuZSks7DCahlDKBlx2sXdYPjNg711TLM0uZ6aFE67SsKgcgm92DF7nUJD202pjdBjGREqo2O7ROGr7ruS/FXIXRzIu3IM2yjN1+TJ4gy7VlorVuK6V84KPZp4JWkKPZwVuYsVorVbSaGEFYb4lyNRRxjcHC/SpDB2N5n8NMccCokP/WO1KzxNTqaLwkm9FRQa2jE1+ghMrqpvZVIYd7xJ4TEPRrW3aCk2w7HnYoWfJ+f6VQotB12clsafUsoBVthaSIPCOOBhUtg7xaX4GJyu/svBRaEhOzo/CxRWOQ/lEnjEmgUOZZMbYOAacqZ3icJ0p2lGRP8IT+akYJtsv/ZHexklomJjII6EJuswLcc+VHEnuk70xznFYXQfNwq+zA9vT4dDJwgib5AIgqDTCbil/nz80by9PWkO+nnnnDbI9KeHjmpEFWichkh+Bq24F4z6/nAYfm8/LM85OZfY20dS1LGkQMm06kPHYo4uRpfFrKBiaqI4/U1uSoXvMckY15zuU0eiCJMjT0RhKs54NmtQf8jZsUgRV5DpMDcaN/0/J42WWklZqCiuP4q+ffxPV367H1ObPowxPysv7agh805xUf/OYUMQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBFGd/wHL4L8b8cEIzAAAAABJRU5ErkJggg==';
      }
      ish.qaMain='--';
      ish.qaMainIcon='';

      if(res.features[i].custom_fields && res.features[i].custom_fields){
        var found=0;
        for(var cf=0;cf<res.features[i].custom_fields.length;cf++){
          if(res.features[i].custom_fields[cf].key==='feature_priority'){
            ish.priority=res.features[i].custom_fields[cf].value;
            if(ish.priority.startsWith('1')){ 
              ish.priority='Blocker';
              ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/blocker.svg';
              found=1;
            }
            else if(ish.priority.startsWith('2')){ 
              ish.priority='High';
              ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/high.svg';
              found=1;
            }
            else if(ish.priority.startsWith('3')){ 
              ish.priority='Critical';
              ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/critical.svg';
              found=1;
            }
            else if(ish.priority.startsWith('4')){ 
              ish.priority='Medium';
              ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/medium.svg';
              found=1;
            }
            else{ 
              ish.priority='Low';
              ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/low.svg';
              found=1;
            }
          }
        }
        if(!found){
          ish.priority='Medium';
          ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/medium.svg'
        }
      }
      if(res.features[i].workflow_status && res.features[i].workflow_status.name){
        ish.status=res.features[i].workflow_status.name;
      }
      ish.summary=res.features[i].name;
      //ish.description=res.features[i].fields.description;
      this.curIssueList.push(ish);
    }
  }
}


export default new AhaTicketStore();