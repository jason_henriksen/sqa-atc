

import {observable,action,reaction } from 'mobx';
import autoBind from 'react-autobind';


class JenkinsStore
{
  constructor(){autoBind(this);}    

  core = {}; // set initially in App.js.  Allows the store to make API calls

  @observable ticketCount=0;
  @observable viewVersion='';
  @observable gitBuildHistoryMap=observable.map({});

  @observable rawBuildList=[];
  @observable jiraBuildHistoryMap=observable.map({});

  // tool to list all branch builders.
  // foreach branch builder load the build list and whats in each build

  //https://jenkins-integ.clearcollateral.com/view/Branch%20Builders/job/ccp-webapp-branch-builder/api/json?pretty=true&tree=builds[id,timestamp,changeSet[items[timestemp,commitId,msg]]]
  @action updateUIBuildList(){ this.core.smartFetchJenkinsGetBranchBuilderHistory("ccp-webapp-branch-builder", (res)=>{ this.updateGitBuildHistoryMap(res); }); }
  @action updateGitBuildHistoryMap(data){ console.log('got git build history',data); this.gitBuildHistoryMap.set('ccp-webapp-branch-builder',data); }
  


  @action updateKnownBuildList(){ this.core.smartFetchJenkinsGetBranchBuilderList( (res)=>{console.log(res);this.rawBuildList=res.jobs;})};

  loadProjectData = reaction(
    () => (this.rawBuildList && this.rawBuildList.length>0),  // if the number of project keys changes (usually only on load, using react() to avoid redundant work
    () => {                              // load each project

    for (var i = 0; this.rawBuildList && i < this.rawBuildList.length; i++) { // loop the projects.  Can't do this in compute for data integrity rules.
      // load each project  (technically, this fetch should be done in a separate auto-run, instead of computed)
      this.core.smartFetchJenkinsGetBranchBuilderHistory(this.rawBuildList[i].name, (name,res)=>{ this.updateJiraBuildHistoryMap(name,res); });
    };
  });

  @action updateJiraBuildHistoryMap(name, res){ console.log(name,'====', res); this.jiraBuildHistoryMap.set(name,res.builds); }


}

export default new JenkinsStore();

