

import {observable,computed,action, autorun,reaction } from 'mobx';
import autoBind from 'react-autobind';
import moment from 'moment';
import { toast } from 'react-toastify';

class GitTicketStore
{
  constructor(){autoBind(this);}    

  core = {}; // set initially in App.js.  Allows the store to make API calls

  // used for release analysis
  @observable ticketCount=0;
  @observable viewVersion='';
  @observable curIssueList=[];
  @observable curTotalCount=0;

  // used for build analysis
  @observable gitPRPile=[];  
  @observable gitCommitPile=observable.map({});  


  @observable milestones={};
  @computed get milestoneNameList(){
    var res=[];
    for (var i = 0; this.milestones && i < this.milestones.length; i++) {
      res.push(this.milestones[i].title);      
    };
    return res;
  };

  @computed get milestoneMap(){
    var res={};
    for (var i = 0; this.milestones && i < this.milestones.length; i++) {
      res[this.milestones[i].title] = this.milestones[i].number;
    }
    return res;
  };



  updateGitCount(){
    this.core.smartFetchGitGet("/repos/ClearCapital/ccp",(res)=>{this.ticketCount=res.open_issues;});
  }

  updateMilestones(){
    this.core.smartFetchGitGet("/repos/ClearCapital/ccp/milestones",(res)=>{this.milestones=res;});
  }

  loadIssueData = autorun(()=>{
    if(this.viewVersion){
      this.updateIssueList(this.viewVersion); // anytime view version is changed, go load the issues for that version
    }
  });

  @action updateIssueList(key){ this.core.smartFetchGitGet("/repos/ClearCapital/ccp/issues?state=all&milestone="+
    this.milestoneMap[this.viewVersion], // computed map of names to ids
    (res)=>{ this.updateIssueListWithResults(res); }); }

  @action updateIssueListWithResults(res){
    if(!res){toast.error("Git Access Error: Missing Results");}

    this.curTotalCount=res.total;
    this.curIssueList.clear();
    var dupeIshPRList=[];
    var qaUserNameListGit=['ap-ccp','bsaetern','clotze','josejro','vgustafson','JasonHenriksen'];
    
    for(var i=0;i<res.length;i++){
      var ish={};
      if(res[i].pull_request){ // Filter out the pull requests.  Only look at issues
        ish.id=Number(res[i].number);
        ish.url='https://github.com/clearcapital/ccp/issues/'+res[i].number;
        ish.key='GITPR-'+res[i].number;
        ish.targetIssue = Number(res[i].title.replace( /(^.+\D)(\d+)(\D.+$)/i,'$2')); 
        if(ish.targetIssue){
          ish.targetURL='https://github.com/clearcapital/ccp/issues/'+ish.targetIssue;
        }
        dupeIshPRList.push(ish.targetIssue);
        if(res[i].title.includes('-')){ ish.summary=res[i].title.substring(res[i].title.indexOf('-')+1);        }
        else{                           ish.summary=res[i].title; }
      }
      else{
        ish.id=Number(res[i].number);
        ish.url='https://github.com/clearcapital/ccp/issues/'+res[i].number;
        ish.key='GIT-'+res[i].number;
        ish.summary=res[i].title;
      }

      // assignee stuff
      if(res[i].assignees && res[i].assignees.length>0){
        // first assignee is owner
        if(res[i].assignees[0]){
          ish.ownerMain=res[i].assignees[0].login;
          ish.ownerMainIcon=res[i].assignees[0].avatar_url;;
        }
        for(var qactr=0;qactr<res[i].assignees.length;qactr++){
          // first qa found is qa owner
          if(qaUserNameListGit.includes(res[i].assignees[qactr].login)){
            ish.qaMain=res[i].assignees[qactr].login;
            ish.qaMainIcon=res[i].assignees[qactr].avatar_url;
          }
        }
      }
      else if(res[i].assignee){
        ish.ownerMain=res[i].assignee.login;
        ish.ownerMainIcon=res[i].assignee.avatar_url;;
    }
      // lables stuff
      if(res[i].labels){
        // default values
        ish.priority='Low';
        ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/low.svg';
        ish.status='In Progress';

        for(var lc=0;lc<res[i].labels.length;lc++){
          if(res[i].labels[lc].name==='blocker'){ 
            ish.priority='Blocker';
            ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/blocker.svg'
          }
          else if(res[i].labels[lc].name==='feature' || res[i].labels[lc].name==='high'){ 
            ish.priority='High';
            ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/high.svg'
          }
          else if(res[i].labels[lc].name==='ClientCritical' || res[i].labels[lc].name==='critical'){ 
            ish.priority='Critical';
            ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/critical.svg'
          }
          else if(res[i].labels[lc].name==='bug' || res[i].labels[lc].name==='medium'){ 
            ish.priority='Medium';
            ish.priorityIcon='https://clearcapital.atlassian.net/images/icons/priorities/medium.svg'
          }
          else if(res[i].labels[lc].name.includes('test')){ 
            ish.status='In QA';
          }
          else if(res[i].labels[lc].name.includes('ready to merge')){ 
            ish.status='Resolved';
          }
        }
      }
      if(res[i].state==='closed'){
        ish.status='Resolved';
      }

      ish.description=res[i].body;
      this.curIssueList.push(ish);
    }

    // copy priority from ticket to target issue
    //debugger;
    for(var r=0;r<this.curIssueList.length;r++){
      if(this.curIssueList[r].targetIssue){ // Only look at pull requests.
        // bonehead search due to no map
        for(var t=0;t<this.curIssueList.length;t++){
          if(this.curIssueList[t].id === this.curIssueList[r].targetIssue){
            // put the soon to be filtered issues' priority onto the PR
            this.curIssueList[r].priority = this.curIssueList[t].priority;
            this.curIssueList[r].priorityIcon = this.curIssueList[t].priorityIcon;

            // search the PR and the issue for an owner
            this.curIssueList[r].ownerMain=(this.curIssueList[t].ownerMain||this.curIssueList[r].ownerMain);
            this.curIssueList[r].ownerMainIcon=(this.curIssueList[t].ownerMainIcon||this.curIssueList[r].ownerMainIcon);

            // search the PR and the issue for a QA
            this.curIssueList[r].qaMain=(this.curIssueList[t].qaMain||this.curIssueList[r].qaMain);
            this.curIssueList[r].qaMainIcon=(this.curIssueList[t].qaMainIcon||this.curIssueList[r].qaMainIcon);    
          }
        }

      }
    }


    // now filter form the issue list any issue that it duplicated by a PR
    this.curIssueList = this.curIssueList.filter( function(ci){
      return (!dupeIshPRList.includes(ci.id));
    });
  }

  // onMount of main compnent, get the list of the last 100 PRs
  @action updateGitPrList(){
    this.core.smartFetchGitGet("/repos/ClearCapital/ccp/pulls?state=all&per_page=100",(res)=>{this.gitPRPile=res;});
  }

  // when updateGitPrList is done, fetch the commit list for each PR less than 6 weeks old.
  loadProjectData = reaction(
    () => (this.gitPRPile && this.gitPRPile.length),  // if the number of project keys changes (usually only on load, using react() to avoid redundant work
    () => {                              // load each PRs commits      
    for (var i = 0; this.gitPRPile && i < this.gitPRPile.length; i++) { // loop the projects.  Can't do this in compute for data integrity rules.
      if( moment(this.gitPRPile[i].created_at).isAfter( moment().subtract(6,'weeks') ) ){
        this.updatePrCommits(this.gitPRPile[i].number);
      }
    };
  });

  // when a PRs commit list comes in, put it into the PR->CommitList map.
  @action updatePrCommits(prNum){
    this.core.smartFetchGitGet("/repos/ClearCapital/ccp/pulls/"+prNum+"/commits",
      (res)=>{
        //this.gitCommitPile[prNum]=res;console.log(prNum,res);

        // just handling this directly since I have the
        for (var i = 0; res && i < res.length; i++) {
          this.gitCommitPile.set( res[i].sha , prNum);
        }
      });
  }



}

export default new GitTicketStore();