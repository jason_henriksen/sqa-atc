

import {observable } from 'mobx';
import autoBind from 'react-autobind';
import { toast } from 'react-toastify';

class CoreStore
{
    constructor(){autoBind(this);}    

    @observable workCount=0;

    corsPort='9000'; // the webapp expects the same hostname to be serving a cors proxy on port 9000
    corsServer=window.location.protocol+"//"+window.location.hostname+':'+this.corsPort;

    // https://clearcapital.atlassian.net
    smartFetchJiraGet(url,handler){
        this.workCount++;
        //http://localhost:8080
        fetch(this.corsServer+url, {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Authorization":"Basic amFzb24uaGVucmlrc2VuQGNsZWFyY2FwaXRhbC5jb206SkpPM0plbTUwSkJla3FobTBiUnVCQUY2",
                "Target-URL":"https://clearcapital.atlassian.net",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        })
        .then(response => response.json()/*response.json()*/) // parses response to JSON
        .catch(error => {this.workCount--;toast.error("Error: "+error);})
        .then(response => {this.workCount--;handler(response);})
    }

    smartFetchJiraPost(url,data,handler)
    {
    }

    smartFetchGitGet(url,handler){
        this.workCount++;
        fetch('https://api.github.com'+url, { // cors friendly, no proxy needed
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Authorization":"Basic amFzb24uaGVucmlrc2VuQGNsZWFyY2FwaXRhbC5jb206ZmM3ODlmNjJhMzlkZDA4MGM5OTU3NWQyY2Q3ZmRlNzUwNDZkYmUzZg==",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        })
        .then(response => response.json()/*response.json()*/) // parses response to JSON
        .catch(error => {this.workCount--;toast.error("Error: "+error);})
        .then(response => {this.workCount--;handler(response);})
    }

    smartFetchGitPost(url,body,handler){}

    smartFetchJenkinsGetBranchBuilderHistory(buildTarget,handler){
        this.workCount++;
        var url="/view/Branch%20Builders/job/"+buildTarget+"/api/json?pretty=true&tree=builds[id,timestamp,changeSet[items[timestemp,commitId,msg]]]";
        fetch(this.corsServer+url, {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Authorization":"Basic amFzb24uaGVucmlrc2VuQGNsZWFyY2FwaXRhbC5jb206YmYwMWVmMjNhNDYwZDEzYmRmYjY4NzY1ODRlMTVlYTE=",
                "Target-URL":"https://jenkins-integ.clearcollateral.com",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        })
        .then(response => response.json()) // parses response to JSON
        .catch(error => {this.workCount--;toast.error("Error: "+error);})
        .then(response => {this.workCount--;handler(buildTarget,response);})
    }

    smartFetchJenkinsGetBranchBuilderList(handler){
        this.workCount++;
        var url="/view/Branch%20Builders/api/json";
        fetch(this.corsServer+url, {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Authorization":"Basic amFzb24uaGVucmlrc2VuQGNsZWFyY2FwaXRhbC5jb206YmYwMWVmMjNhNDYwZDEzYmRmYjY4NzY1ODRlMTVlYTE=",
                "Target-URL":"https://jenkins-integ.clearcollateral.com",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        })
        .then(response => response.json()) // parses response to JSON
        .catch(error => {this.workCount--;toast.error("Error: "+error);})
        .then(response => {this.workCount--;handler(response);})
    }

    smartFetchAhaGet(url,handler){
        this.workCount++;
        fetch(this.corsServer+url, {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Authorization":"Bearer 7139253568e63d27a3430f66dfdf5da0d84e8e0f564d9045e7492c15c25cbf62",
                "Target-URL":"https://clearcapital.aha.io",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        })
        .then(response => response.json()/*response.json()*/) // parses response to JSON
        .catch(error => {this.workCount--;toast.error("Error: "+error);})
        .then(response => {this.workCount--;handler(response);})
    }

}

export default new CoreStore();