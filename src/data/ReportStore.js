

import {computed } from 'mobx';
import autoBind from 'react-autobind';


class ReportStore
{
    constructor(){autoBind(this);}    

    @computed get percentByStatus()
    {
      if(!this.GitStore || !this.JiraStore){
        return {};
      }
      var res={
        other:0,
        dev:0,
        qa:0,
        done:0,
        total:0,
      };
      var ish =null;
      for(var i=0;i<this.JiraStore.curIssueList.length;i++){
        ish=this.JiraStore.curIssueList[i];
        if('Resolved'===ish.status || 'Closed'===ish.status){res.done++;}
        else if('In QA'===ish.status){res.qa++;}
        else if('In Progress'===ish.status){res.dev++;}
        else {res.other++;}
        res.total++;
      }
      for(var g=0;g<this.GitStore.curIssueList.length;g++){
        ish=this.GitStore.curIssueList[g];
        if('Resolved'===ish.status || 'Closed'===ish.status){res.done++;}
        else if('In QA'===ish.status){res.qa++;}
        else if('In Progress'===ish.status){res.dev++;}
        else {res.other++;}
        res.total++;
      }  
      // Needs Aha store numbers added here
      return res;
    }

    @computed get percentByType()
    {
      if(!this.GitStore || !this.JiraStore){
        return {};
      }
      var res={
        other:0,
        blocker:0,
        critical:0,
        high:0,
        medium:0,
        low:0,
        total:0
      };
      var ish =null;
      for(var i=0;i<this.JiraStore.curIssueList.length;i++){
        ish=this.JiraStore.curIssueList[i];
        if('Blocker'===ish.priority){res.blocker++;}
        else if('Critical'===ish.priority){res.critical++;}
        else if('High'===ish.priority){res.high++;}
        else if('Medium'===ish.priority){res.medium++;}
        else if('Low'===ish.priority){res.low++;}
        else {res.other++;}
        res.total++;
      }
      for(var g=0;g<this.GitStore.curIssueList.length;g++){
        ish=this.GitStore.curIssueList[g];
        if('Blocker'===ish.priority){res.blocker++;}
        else if('Critical'===ish.priority){res.critical++;}
        else if('High'===ish.priority){res.high++;}
        else if('Medium'===ish.priority){res.medium++;}
        else if('Low'===ish.priority){res.low++;}
        else {res.other++;}
        res.total++;
      }  
      return res;
    }

}

export default new ReportStore();