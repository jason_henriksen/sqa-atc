import React, { Component } from 'react';

import {Provider} from 'mobx-react';
//import autoBind from 'react-autobind';

import CoreStore from './data/CoreStore';
import GitTicketStore from './data/GitTicketStore';
import JiraTicketStore from './data/JiraTicketStore';
import AhaTicketStore from './data/AhaTicketStore';
import ReportStore from './data/ReportStore';
import JenkinsStore from './data/JenkinsStore';

import ToolChooser from './ui/ToolChooser';
import Overlay from './ui/Overlay';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

//styles
import './App.scss';
import './App.less';
import './App.styl';

import './patterns.css';

// giving fetch access to the other stores
GitTicketStore.core = CoreStore;
JiraTicketStore.core = CoreStore;
AhaTicketStore.core = CoreStore;
JenkinsStore.core = CoreStore;

ReportStore.GitStore = GitTicketStore;
ReportStore.JiraStore = JiraTicketStore;
ReportStore.AhaStore = AhaTicketStore;

const stores = {CoreStore,GitTicketStore,JiraTicketStore,AhaTicketStore,ReportStore,JenkinsStore}

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider {...stores}>
          <div>
            <ToastContainer />
            <Overlay>
              <ToolChooser/>
            </Overlay>
          </div>
        </Provider>
      </div>
    );
  }
}


